# General variables
variable "location"                  {default = "UK South"}
variable "name_prefix"               {default = "adam"}
variable "resource_group_name"       {default = "adam"}
variable "admin_username"            {default = "adamrummer"
  description = " The admin username for all machines created"
}
variable "admin_password"            {default = "P@ssw0rd!"
  description = "The admin password to be used on all VMs that will be deployed.
   The password must meet the complexity requirements of Azure"
}
variable "tags"                                   {
  description = "A map of the tags to use on all resources"
  type = "map"
  default = { "Name" = "adam" }
}



# VNET specific variables
variable "subnet_names"              {default = [ "subnet1" ]}
variable "nsg_ids"                   {
  type = "map"
  default = { "subnet1" = "nsgid1" }
}

# ------------------ Computegroup specific variables --------------------------
variable "lb_backend_address_pool_ids"  {
  default = ""
  description = "The id of the backend address pools of the loadbalancer to
  which the VM scale set is attached"
}
variable "vnet_subnet_id"               {
  default = ""
  description = "The admin password to be used on the VMSS that will be deployed.
   The password must meet the complexity requirements of Azure"
}
variable "cmd_extension"                 {
  default = ""
  description = "Command to be excuted by the custom script extension"
}
variable "lb_port"                       {
  default = "[frontend_port, protocol, backend_port]"
  description = "Protocols to be used for lb health probes and rules.
   [frontend_port, protocol, backend_port]"
}
variable "managed_disk_type"              {
  default = "Standard_LRS"
  description = "Type of managed disk for the VMs that will be part of this
  compute group. Allowable values are 'Standard_LRS' or 'Premium_LRS'."
}
variable "nb_instance"                    {
  default = "1"
  description = "Specify the number of vm instances"
}
variable "network_profile"                {
  default = ""
  description = "The name of the network profile that will be used in the VM
  scale set"
}
variable "ssh_key"                        {
  default = "~/.ssh/id_rsa.pub"
  description = "Path to the public key to be used for ssh access to the VM"
}
variable "vm_os_offer"                    {
  default = "CentOS"
  description = "The name of the offer of the image that you want to deploy"
}
variable "vm_os_publisher"                {
  default = "OpenLogic"
  description = "The name of the publisher of the image that you want to deploy"
}
variable "vm_os_sku"                      {
  default = "7.3"
  description = "The sku of the image that you want to deploy"
}
variable "vm_os_version"                  {
  default = "latest"
  description = "The version of the image that you want to deploy."
}
variable "vm_size"                        {
  default = "Standard_B1s"
  description = "Size of the Virtual Machine based on Azure sizing"
}
variable "vmscaleset_name"                {
  default = "VMscaleset"
  description = "The name of the VM scale set that will be created in Azure"
}
# variable "vm_os_simple"                  {default = ""
#   description = "Specify Ubuntu or Windows to get the latest version of each os"
# }
# variable "vm_os_id"                      {default = ""
#   description = "The ID of the image that you want to deploy if you are using
#   a custom image."
# }
