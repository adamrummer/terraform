module "network-security-group" {
  source  = "Azure/network-security-group/azurerm"
  version = "1.1.1"

  resource_group_name        = "${var.resource_group_name}"
  location                   = "${var.location}"
  security_group_name        = "${var.name_prefix}-NSG"
}

module "vnet" {
  source  = "Azure/vnet/azurerm"
  version = "1.2.0"

  location            = "${var.location}"
  vnet_name           = "${var.name_prefix}-VNET"
  subnet_names        = "${var.subnet_names}"
  nsg_ids             = "${var.nsg_ids}"
}


module "computegroup" {
  source  = "Azure/computegroup/azurerm"
  version = "1.1.0"

  load_balancer_backend_address_pool_ids = "${var.load_balancer_backend_address_pool_ids}"
  vnet_subnet_id                         = "${var.vnet_subnet_id}"
  admin_password                         = "${var.admin_password}"
  admin_username                         = "${var.admin_username}"
  cmd_extension                          = "${var.}"
  computer_name_prefix                   = "${var.name_prefix}"
  lb_port                                = "${var.lb_port}"
  location                               = "${var.location}"
  managed_disk_type                      = "${var.managed_disk_type}"
  nb_instance                            = "${var.nb_instance}"
  network_profile                        = "${var.network_profile}"
  resource_group_name                    = "${var.resource_group_name}"
  ssh_key                                = "${var.ssh_key}"
  tags                                   = "${var.tags}"
  # vm_os_id                               = "${var.vm_os_id}"
  vm_os_offer                            = "${var.vm_os_offer}"
  vm_os_publisher                        = "${var.vm_os_publisher}"
  # vm_os_simple                           = "${var.vm_os_simple}"
  vm_os_sku                              = "${var.vm_os_sku}"
  vm_os_version                          = "${var.vm_os_version}"
  vm_size                                = "${var.vm_size}"
  vmscaleset_name                        = "${var.vmscaleset_name}"

}
