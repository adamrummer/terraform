#!/bin/bash

# Installs httpd, sets it to run on start up and makes folder writeable
yum install -y httpd
systemctl enable httpd
systemctl restart httpd
chmod 777 /var/www/html/

# Sets index.html file to welcome users
# Use $ but no need for quotes because of the __END__ thingy
cat >/var/www/html/index.html << __END__

<!DOCTYPE html>
<html>
<body>

<h1>This webserver works.</h1>

<p>Enjoy your stay!</p>

<IMG SRC="http://www.reactiongifs.com/wp-content/uploads/2013/01/not-mad-amazed.gif">

</body>
</html>
__END__

chmod 644 /var/www/html/index.html
chmod 755 /var/www/html/
