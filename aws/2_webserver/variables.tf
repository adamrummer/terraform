
# ------------------------- universal variables --------------------------------
# unique identifying prefix name to assign to all objects
variable "prefix" = {default = "adam"}

# ---------------------- EC2 instance variables -------------------------------
variable "access_key" {} # You must run the mkenv.sh script to (secretly)
variable "secret_key" {} # populate these two variables.
variable "region" {
  default = "eu-west-2"}
variable "associate_public_ip_address" {
  default = true}
variable "image_id" {
  default = "ami-0274e11dced17bb5b"}
variable "instance_type" {
  default = "t2.micro"}
variable "key_name" {
  default = "AdamRummer"}
variable "ssh_key_location" {
  default = "~/.aws/${key_name}.pem"} # on local machine
variable "subnet_id" {
  description = "subnet to launch the instance in"
  default = ""}
# ---------- s3 bucket variables for external tf state location  --------------
variable "path_to_state_file" {
  default = "babbyadom/badam_(tsch)"}
variable "bucket_name" {
  default = "badadom-buck"}
