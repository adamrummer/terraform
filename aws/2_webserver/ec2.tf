

resource "aws_instance" "ec2" {
  ami                         = "${var.image_id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_name}"
  security_groups             = [ "al-office" ]
  associate_public_ip_address = "${var.associate_public_ip_address}"
  subnet_id                   = "${var.subnet_id}"
  tags {
    Name = "${var.prefix}-ec2"
  }
  # Provisioning the instance to be a web server
  user_data = "${file("./shellscripts/apachescript.sh")}"
}

# Save the public IP for testing
provisioner "local-exec" {
  command = "echo ${aws_instance.ec2.public_ip}"
}
output "public_ip" {
 value = "${aws_instance.ec2.public_ip}"
}
