provider "aws" {
  access_key                  = "${var.access_key}"
  secret_key                  = "${var.secret_key}"
  region                      = "${var.region}"
}

resource "aws_instance" "ec2" {
  ami                         = "${var.image_id}"
  instance_type               = "t2.micro"
  key_name                    = "AdamRummer"
  security_groups             = [ "al-office" ]
  associate_public_ip_address = true
  tags {
    Name = "adamTFvm"
  }
}
