resource "aws_db_instance" "default" {
  allocated_storage    = 5
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "${var.db_name}"
  username             = "${var.db_username}"
  password             = "${var.db_password}"
  parameter_group_name = "default.mysql5.7"
  db_subnet_group_name = "${aws_db_subnet_group.default.name}"
  publicly_accessible  = true
  identifier           = "${var.db_name}"
  skip_final_snapshot  = true
  tags {
    Name = "${var.db_name}"
  }
}

resource "aws_db_subnet_group" "default" {
  name       = "main"
  subnet_ids = "${var.subnet_id_list}"
  tags {
    Name = "${var.db_name}_subnet_group"
  }
}
