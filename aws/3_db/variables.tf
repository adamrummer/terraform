variable "access_key" {}
variable "secret_key" {}

# ------------------------- Database variables --------------------------------
variable "db_name" {
  default = "thewonderfulworldofdatastorage"
}
variable "subnet_id_list" {
  default = ["subnet-8a08f3e3",
             "subnet-b6bfa5ce",
             "subnet-0de6f5120aa897fd2"]
}
variable "db_username" {
  default = "badadom"
}
variable "db_password" {
  default = "secretsecret"
}
variable "region" {
  default = "eu-west-2"
}

# ------------------------- S3 bucket variables -------------------------------
# backend does not work with interpolation
# variable "bucket_name" {
#   description = "name of bucket to store and retrieve state file from"
#   default     = "badadom-buck"
# }
# variable "path_to_state_file" {
#   description = "path once inside of bucket"
#   default     = "db+webserver/"
# }
