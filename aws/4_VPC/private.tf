/*
  Private Subnet
*/
resource "aws_subnet" "eu-west-2-private" {
    vpc_id = "${aws_vpc.default.id}"

    cidr_block = "${var.private_subnet_cidr}"
    availability_zone = "eu-west-2a"

    tags {
        Name = "Badadom Private Subnet"
    }
}

resource "aws_route_table" "eu-west-2-private" {
    vpc_id = "${aws_vpc.default.id}"

    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = "${aws_nat_gateway.gw.id}"
    }

    tags {
        Name = "Badadom Private Subnet"
    }
}

resource "aws_route_table_association" "eu-west-2-private" {
    subnet_id = "${aws_subnet.eu-west-2-private.id}"
    route_table_id = "${aws_route_table.eu-west-2-private.id}"
}
